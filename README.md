# Enview DevOps Challenge

The requirements for this challenge are: 

1. Create a Jenkins Docker and run locally or on a server of your choice. 
2. Within your Jenkins docker, create a Jenkins file to pull from a GitHub repo of an nginx
hello world docker.
3. Create a Jenkins project to do the build.
4. Have Jenkins deploy Docker image (to local computer or remote server of your
choice) using any technique you prefer.


# Prerequisites:
- Docker is installed in machine running the code
- Ports 8080 and 8081 are available in host

# To run:

- git clone the project or download then unzip
- inside the project, run: 
   ```docker build -t enview_challenge . ``` 
- once the build is done, run this command: 
```docker run --rm -u root -p 8080:8080 -v $(which docker):/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock -v $PWD/jenkins_home/jobs:/var/jenkins_home/jobs enview_challenge```
-- this command uses the docker installed in host machine and makes the same docker install available for the Jenkins container. Also uses a local jobs folder which houses the project to pull the Nginx hello world docker image
- Visit ```localhost:8080``` and click on Nginx project. Click 'Build Now'. 
- If build is successful, visit ```localhots:8081``` to see Nginx page