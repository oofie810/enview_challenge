# use jenkins lts image as base
FROM jenkins/jenkins:lts

# do not show setup wizard anymore since we will manually install plugins
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

# Once jenkins is running and configured, run the following command to find the list of plugins installed:
RUN /usr/local/bin/install-plugins.sh \
  credentials \
  credentials-binding \
  display-url-api \
  pipeline-build-step \
  pipeline-model-api \
  pipeline-model-declarative-agent \
  pipeline-model-definition \
  pipeline-model-extensions \
  pipeline-rest-api \
  pipeline-stage-step \
  pipeline-stage-tags-metadata \
  pipeline-stage-view 

USER root

USER jenkins